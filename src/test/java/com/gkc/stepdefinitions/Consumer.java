package com.gkc.stepdefinitions;

import com.gkc.main.common.TokenManagementBatchClient;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.Assert;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class Consumer implements Runnable{

    private static final String BATCH_FOLDER_PKI = "src/test/resources/data/token/SG_OSPL/PKI_Token/TokenManagementServer";
    private static final String BATCH_FOLDER_OTP = "src/test/resources/data/token/SG_OSPL/OTP_Token/TokenManagementServer";
    private static final String BATCH_FOLDER_VTAP = "src/test/resources/data/token/SG_OSPL/OTP_Token/AuthenticationServer";
    private static final String PSKC_KEY_PACKAGE = "pskc:KeyPackage";
    private BaseService apiTest;

    @Override
    public void run() {
        try{
            batchValidationIsTriggered();
        } catch (InterruptedException | ParserConfigurationException | SAXException | IOException e) {
            apiTest.logInReport(e.getMessage());
            Assert.fail(e.getMessage());
        }
    }

    private void batchValidationIsTriggered() throws ParserConfigurationException, IOException, SAXException {
        apiTest.logInReport("Consumer Thread Started");
        String batchDescription = apiTest.getPropertyValue(TokenManagementBatchClient.BATCH_DESCRIPTION);
        File folder = null;
        String fileName = null;
        if(batchDescription.equals("PKI"))
            folder = new File(BATCH_FOLDER_PKI);
        else if (batchDescription.equals("OTP"))
            folder = new File(BATCH_FOLDER_OTP);
        else if (batchDescription.equals("VTAP"))
            folder = new File(BATCH_FOLDER_VTAP);

        File[] listofFiles = folder.listFiles();
        if(listofFiles.length==2)
            fileName=listofFiles[1].getName();
        else
            fileName=listofFiles[0].getName();

        apiTest.logInReport("Validating: "+fileName);
        Document document = readFileIntoDocument(new File(folder+"\\"+fileName));
        String fileType = fileName.substring(0,4);
        int status = 1;
        Integer recordsCount = document.getElementsByTagName(PSKC_KEY_PACKAGE).getLength();
        apiTest.setPropertyUsingValue(TokenManagementBatchClient.BATCH_RECORDS_COUNT, recordsCount.toString());
        apiTest.setPropertyUsingValue(TokenManagementBatchClient.BATCH_FILE_TYPE, fileType);
        if(fileType.contains("APIN")){
            apiTest.setPropertyUsingValue(TokenManagementBatchClient.BATCH_TEMP_TABLE, TokenManagementBatchClient.BATCH_TEMP_TABLE_APIN);
            apiTest.setPropertyUsingValue(TokenManagementBatchClient.BATCH_FILE_TYPE, fileType);
            status = validateAPIRecords(document);
        }
    }

    private int validateAPIRecords(Document document) {
        consumerKafkaTopic();
        String batchId = apiTest.getPropertyValue(TokenManagementBatchClient.BATCH_ID);
    }

    private void consumerKafkaTopic() {
        int KafkaTopicCount = Integer.parseInt(apiTest.getPropertyValue(TokenManagementBatchClient.BATCH_RECORDS_COUNT))/TokenManagementBatchClient.KAFKA_BATCH_SIZE;
        Properties prop = new Properties();

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(prop);
        consumer.subscribe(Collections.singletonList(TokenManagementBatchClient.KAFA_TOPIC_NAME));
        consumer.poll(Duration.ofSeconds(15));

        ConsumerRecords<String, String> records;
    }

    private Document readFileIntoDocument(File xmlFile) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbFactory.setNamespaceAware(false);
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();
        return doc;
    }
}
