package com.gkc.stepdefinitions;

import com.gkc.main.common.Constants;
import com.gkc.main.common.Helper;
import com.google.gson.JsonObject;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Properties;

public class BaseService {


    private Properties property;
    private Scenario scenario;
    private RequestSpecBuilder rsb;
    private JsonObject body;
    private HashMap rsbHeaders;
    private Response responseString;

    public Properties getProperty(){
        return property;
    }

    public String setProperty(String propertyName){
        return property.getProperty(propertyName);
    }

    @Before
    public void init(Scenario scenario){
        this.scenario = scenario;
        rsb = new RequestSpecBuilder();
        body = new JsonObject();
        property = Helper.loadProperties();
    }

    @Given("Common headers are provided")
    public void common_headers_are_provided(){
        createHeader(Constants.HEADERS);
    }

    @Then("Validate error code is {string}")
    public void validate_status_errorcode_is(String errorCode){
        Assert.assertEquals(errorCode, responseString.jsonPath().get("errorCode"));
    }

    private void createHeader(String header) {
        String[] headers = header.split(",");
        rsbHeaders = new HashMap();
        for (String h : headers) {
            String[] hv = h.split(":");
            if(hv.length==2){
                if(hv[0].contains("x-correlation-id")){
                    rsbHeaders.put(hv[0], Helper.getCorrectionID());
                } else if(hv[0].contains("x-source-date-time")){
                    rsbHeaders.put(hv[0], Helper.getDateTime());
                }
                else{
                    rsbHeaders.put(hv[0], hv[1]);
                }
            }else {
                rsbHeaders.put(hv[0], "");
            }
        }
    }

    public void logInReport(String logText) {
        scenario.log(logText);
    }

    public String getPropertyValue(String propertyName){
        return property.getProperty(propertyName);
    }

    public void setPropertyUsingValue(String propertyName, String propertyValue) {
        property.remove(propertyName);
        property.setProperty(propertyName, propertyValue);
    }
}
