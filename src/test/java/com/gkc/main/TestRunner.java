package com.gkc.main;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/feature",
        glue = "com.gkc.stepdefinitions",
        plugin = {
                "json:target/cucumber-report/cucumber.json",
                "pretty",
                "html:target/html",
                "json:target/json/file.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
                "com.gkc.email.EmailConcurrentEventListener:"
        },
        monochrome = true,
        tags = {"@Test"}
)
public class TestRunner {
}
