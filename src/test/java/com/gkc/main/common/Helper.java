package com.gkc.main.common;

import com.google.gson.JsonArray;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Helper {
    private static String RES_FILE="\\src\\test\\resources\\data\\test_data_file.xlsx";
    private static String CONFIG="\\src\\test\\resources\\config.properties";

    private static Properties prop=  new Properties();

    public static String validateResponseWithSchema(String responseText, String schemaName){
        boolean valid = true;
        StringBuilder message = new StringBuilder();
        try{
            JSONObject jsonSchema = new JSONObject(
                    new JSONTokener(new FileInputStream(Helper.getFilePath(schemaName))));
            System.out.println(responseText);
            JSONObject jsonSubject = new JSONObject(new JSONTokener(responseText));
            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonSubject);
        } catch (ValidationException exception) {
            List<ValidationException> exceptionList = exception.getCausingExceptions();
            for (ValidationException e : exceptionList) {
                System.out.println(e.getMessage());
                message.append(e.getMessage());
            }
            if(exceptionList.size()==0){
                message.append(exception.getMessage());
            }
        }
        catch (FileNotFoundException e){
            message.append(e.getMessage());
        }
        return message.toString();
    }

    private static File getFilePath(String fileName) {
        return new File(System.getProperty("user.dir"+File.separator+fileName));
    }

    public static Properties loadProperties(){
        if(prop.isEmpty()){
            try(InputStream input = new FileInputStream(getFilePath(CONFIG))){
                prop.load(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return prop;
    }

    public static String getBasicAuth(String cid, String cpass){
        String auth= cid+":"+cpass;
        String eAuth= Base64.getEncoder().encodeToString(auth.getBytes());
        String key = "Basic "+new String(eAuth);
        return key;
    }

    public static int[] convertJsonArrayToIntegerArray(JsonArray array){
        int [] iArray = new int[array.size()];
        for(int i =0; i<array.size();i++){
            iArray[i] = array.get(i).getAsInt();
        }
        return iArray;
    }

    public static String getCorrectionID(){
        return UUID.randomUUID().toString();
    }

    public static int getRandomId(){
        Random random = new Random(System.currentTimeMillis());
        return 100000+ random.nextInt(20000);
    }

    public static String getUUID(){
        String correlationID= UUID.randomUUID().toString();
        return correlationID;
    }

    public static String getDateTime(){
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss");
        String sourceDateTime = date.format(formatter);
        return sourceDateTime;
    }

    public static String readJSONFromFile(String filepath) throws IOException{
        return new String(Files.readAllBytes(Paths.get(filepath)));
    }
}
