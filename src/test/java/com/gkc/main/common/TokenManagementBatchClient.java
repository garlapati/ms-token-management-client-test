package com.gkc.main.common;

public interface TokenManagementBatchClient {

    String BATCH_RECORDS_COUNT = "BATCH_RECORDS_COUNT";
    String BATCH_FILE_TYPE = "BATCH_FILE_TYPE";
    String BATCH_TEMP_TABLE = "BATCH_TEMP_TABLE";
    String BATCH_TEMP_TABLE_APIN = "BATCH_TEMP_TABLE_APIN";
    String BATCH_DESCRIPTION = "BATCH_DESCRIPTION";
    String SECURITY_PROTOCOL = "security.protocol";
    String SECURITY_PROTOCOL_VAL = "SASL_SSL";
    String SASL_JAAS_CONFIG = "sasl.jaas.config";
    String BATCH_ID = "batchId";
    Integer KAFKA_BATCH_SIZE = 50;
    String KAFA_TOPIC_NAME = "SG_OCBC_TKN_MGMT_BTH_TEST";
}
