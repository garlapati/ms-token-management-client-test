package com.gkc.main.common;

public interface Constants {
    String METHOD_POST="POST";
    String HEADER_ACCESS_TOKEN="x-acc-op";
    String HEADER_REFRESH_TOKEN="x-ref-op";
    int HTTP_STATUS_CODE_OK=200;
    int HTTP_STATUS_CODE_BADREQUEST=400;
    String DELIMITER="_";
    String CLIENT_ID = "client_id";
    String CLIENT_ID_VALUE="1cf5b01e-49df-4afe-9220-705b91068f3e";
    String CLIENT_SECRET="client_secret";
    String CLIENT_SECRET_VAL="1iTVuPEWD5";
    String CLIENT_NAME="client_name";
    String CLIENT_SCOPE="client_scope";
    String CLIENT_TYPE="client_type";
    String METHOD_GET="GET";
    String HEADERS="Content-Type:application/json, xsource-country:SG, x-source-date-time:31031985, x-correlation-id:1234567, x-source-id:BV";
    String SCOPE="scope";
    String AUTH_MS="AUTH";
    String REGISTER_CLIENT_SERVICE="register_client_api";
    String API_KEY="API_KEY";
    String AUTHORIZE_SERVICE="authorize_api";
    String API_KEY_VAL="Basic Mzg4N2Y5MmMtZTc0NC00ZTQ0LT";
}
